from django.db import models
from django.contrib.postgres.fields.jsonb import JSONField


class InputsData(models.Model):
    json_data = JSONField(db_column='json_data', verbose_name='JSON Data')

    def __str__(self):
        return self.json_data

    class Meta:
        db_table = 'inputs_data'
        verbose_name = 'inputs_data'
        verbose_name_plural = 'inputs_data'

