from django import forms


class InputForm(forms.Form):
    text = forms.CharField(label='Text', required=False)