import json
from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponse
from django.core import serializers
from task.forms import InputForm
from task.models import InputsData


class InputsFormView(TemplateView):
    template_name = 'inputs.html'
    form = None

    def get(self, request, *args, **kwargs):
        self.form = InputForm()
        return super(InputsFormView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(InputsFormView, self).get_context_data(**kwargs)
        context['form'] = self.form
        return context

    def post(self, request, *args, **kwargs):
        try:
            data_dict = dict(request.POST)
            del data_dict['csrfmiddlewaretoken']
            json_data = json.dumps(data_dict)
            InputsData.objects.create(json_data=json_data)
        except Exception:
            raise HttpResponseBadRequest
        return redirect('main')


def getJsonData(request, **kwargs):
    try:
        json_data = serializers.serialize("json", InputsData.objects.all())
    except Exception:
        error = 'Failed to create json file'
    else:
        return HttpResponse(json_data, content_type='application/json', status=200)
    return JsonResponse({'Success': False, 'Error': error})
