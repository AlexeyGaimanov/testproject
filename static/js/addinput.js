/**
 * Created by alexey on 12/6/16.
 */

(function($) {
    $(document).ready(function() {
        $('.add-input').on('click', function() {
            var $inputs = $('form > input[id]');
            var addedIndex = $inputs.length;
            var firstInputName = $inputs.first().attr('name');
            var $newLabel = $('label').first().clone()
                .attr('for', 'id_' + firstInputName + addedIndex)
                .text(firstInputName + addedIndex)
                .css('textTransform', 'capitalize');
            var $newInput = $('input[id]').first().clone().attr({
                'name': firstInputName + addedIndex,
                'id': 'id_' + firstInputName + addedIndex
            });
            $inputs.last().after($newInput).after($newLabel);
        });
    });
})(jQuery);